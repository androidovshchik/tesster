package com.example.tesster

import android.graphics.Color
import android.os.Bundle
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.example.tesster.extension.toolbar
import com.example.tesster.model.DescModel
import org.jetbrains.anko.*

class DescActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val desc = intent.getSerializableExtra("desc") as DescModel
        setContentView(scrollView {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
            verticalLayout {
                setSupportActionBar(toolbar(R.style.ThemeOverlay_AppCompat_Dark) {
                    title = desc.name
                    setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.colorPrimary))
                })
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                textView {
                    text = desc.desc
                    textSize = 16f
                    textColor = Color.BLACK
                    setPadding(dip(16), dip(16), dip(16), dip(16))
                }.lparams(matchParent, 0, 1f)
            }.lparams(matchParent, wrapContent)
        })
    }
}